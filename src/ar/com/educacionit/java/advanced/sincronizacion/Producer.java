/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.educacionit.java.advanced.sincronizacion;

import static javafx.beans.binding.Bindings.and;

/**
 *
 * @author Instructor
 */
public class Producer implements Runnable {

    public Q q;

    public Producer(Q q) {
        this.q = q;
        new Thread(this, "Producer").start();
    }

    public void run() {
        int i = 0;
        while (true && i < 100) {
            q.put(i++);
        }
    }
}
